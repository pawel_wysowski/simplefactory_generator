﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            generateRandWebsite();
        }

        public enum WebsiteType
        {
            Photo,
            Address,
            News,
            Info
        };


        void generateRandWebsite()
        {
            Random r = new Random();
            HtmlFactory factory = new HtmlFactory();
            HtmlSite site = factory.generateCode((WebsiteType)r.Next(0,4));
            Uri uri = new Uri(site.getPath());
            webBrowser1.Url = uri;
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
