﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class Photo:HtmlSite
    {
        string path = Path.GetTempPath();
        
        public Photo()
        {
            _Code = "<img src=https://www.wykop.pl/cdn/c3201142/comment_RxHXO30jvLo7Ht7y9Lc3DGVdaVX3kdI0.jpg>" + "<br>";
            using (var tw = new StreamWriter(path+"photo.html"))
            {
                tw.WriteLine(_Code);
                tw.Close();
            }

            _Path = path + "photo.html";
        }
    }
}
