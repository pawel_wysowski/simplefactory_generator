﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    class Address: HtmlSite
    {

        string[] cities = { "Warsaw", "Katowice", "Lodz", "Gdansk" };
        string path = Path.GetTempPath();

        public Address()
        {
            Random r = new Random();

            string number = "";

            for(int i = 0; i<=9; i++)
            {
                number += r.Next(1, 9);
            }


            _Code = "<div align = \"center\">" + cities[r.Next(0,3)]+
              "<p>"+number+"</p>"+"</div>";
            using (var tw = new StreamWriter(path+"address.html"))
            {
                tw.WriteLine(_Code);
                tw.Close();
            }

            _Path = path + "address.html";

        }

    }
}
