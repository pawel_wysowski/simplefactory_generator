﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    class Info:HtmlSite
    {

        public Info()
        {

            string path = Path.GetTempPath();
            _Code = "<div align = \"justify\">" + "<p>" + "<p>As of November 13, the world&rsquo;s most popular digital distribution platform &ndash; Steam, will begin to determine prices in zloty! The introduction of prices expressed in Polish currency will make all games become less expensive. This information was provided to Steve by Valve. <br />If you believe in previous announcements, the conversion rate will be very favorable, though not as attractive as in countries like Russia. For some time in the network are circulating information that 0.99 euros will be converted to 3.59 zlotys.</p>" + "</p>" + "</div>";
            using (var tw = new StreamWriter(path + "Info.html"))
            {
                tw.WriteLine(_Code);
                tw.Close();
            }
            _Path = path + "Info.html";
        }

    }
}
