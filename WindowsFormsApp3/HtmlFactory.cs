﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class HtmlFactory
    {

        public HtmlSite generateCode(Form1.WebsiteType websiteType)
        {
            if (Form1.WebsiteType.Photo.Equals(websiteType))
            {
                return new Photo();
            }else if (Form1.WebsiteType.Address.Equals(websiteType))
            {
                return new Address();
            }
            else if (Form1.WebsiteType.News.Equals(websiteType))
            {
                return new News();
            }
            else if (Form1.WebsiteType.Info.Equals(websiteType))
            {
                return new Info();
            }
            else return null;
        }

    }
}
